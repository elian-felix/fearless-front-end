window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        console.log(data);
        const selectTag = document.getElementById('location');
        for (let loc of data.locations) {
            // Create an 'option' element
            const option = document.createElement('option');

            option.value = loc.id;
            option.innerHTML = loc.name;

            // Append the option element as a child of the select tag
            selectTag.appendChild(option);
        }
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        console.log('Handling the form submit');
        event.preventDefault();

        const form = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(form));
        console.log('json of form:');
        console.log(json);

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation);
        }
    });
});

