function createCard(name, description, pictureUrl, start_d, end_d, locName) {
    const placeholder = document.createElement('div')
    placeholder.className += 'col grid-item'
    placeholder.innerHTML = ` 
   <div class="card shadow">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2">${locName}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        ${start_d} - ${end_d}
      </div>
    </div>
  `;
    return placeholder
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do when the response is bad
            console.log(response.status)
        } else {
            const data = await response.json();

            let title, desc, imgUrl, start_d, end_d, locName;
            const row = document.querySelector('.row');
            row.className += ' gy-3';
            row.innerHTML = '';

            const placeholder = document.createElement('div')
            placeholder.className += 'col grid-item'
            placeholder.innerHTML = `
                <div class="card" aria-hidden="true">
                    <img src="..." class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title placeholder-glow">
                            <span class="placeholder col-6"></span>
                        </h5>
                        <p class="card-text placeholder-glow">
                        <span class="placeholder col-7"></span>
                        <span class="placeholder col-4"></span>
                        <span class="placeholder col-4"></span>
                        <span class="placeholder col-6"></span>
                        <span class="placeholder col-8"></span>
                        </p>
                    </div>
                    <div class="card-footer">
                        <span class="placeholder col-7"></span>
                    </div>
                </div>`;
            for (let _ of data.conferences) {
                row.appendChild(placeholder.cloneNode(true))
            }

            for (let [indx, conference] of data.conferences.entries()) {
                title = conference.name;

                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log(details);
                    desc = details.conference.description;
                    imgUrl = details.conference.location.picture_url;
                    start_d = new Date(details.conference.starts).toDateString();
                    end_d = new Date(details.conference.ends).toDateString();
                    locName = details.conference.location.name;
                }
                const html = createCard(title, desc, imgUrl, start_d, end_d, locName);
                row.replaceChild(html, row.children[indx]);
            }
        }
    } catch (e) {
        // Figure out what to do if an error is raised
        console.log(e)
        let contentContainer = document.querySelector('.container');
        let alert =  document.createElement('div');
        alert.className = 'row';
        alert.innerHTML = `
            <div class="alert alert-danger d-flex align-items-center" role="alert">
                <div>
                    ${e.message}
                </div>
            </div>
        `
        contentContainer.insertBefore(alert, contentContainer.children[1])
    }

});
