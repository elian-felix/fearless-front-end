import React, { useState, useEffect } from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);

    const [success, setSuccess] = useState(false);
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [max_presentations, setMax_presentations] = useState('');
    const [max_attendees, setMax_attendees] = useState('');
    const [location, setLocation] = useState('');

    const [conference, setConference] = useState('');

    const handleFieldChange = (e) => {
        const fieldName = e.target.name[0].toUpperCase() + e.target.name.slice(1);
        const value = e.target.value;
        eval(`set${fieldName}(value)`);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.description = description;
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.max_presentations = max_presentations;
        data.max_attendees = max_attendees;
        data.location = location;

        console.log(data);
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            setSuccess(true);
            setConference(newConference);

            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMax_presentations('');
            setMax_attendees('');
            setLocation('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            {success ? 
            <div className="alert alert-success d-flex align-items-center" role="alert">
                <div>
                  `Congratulations: created new conference {conference.name}`
                </div>
            </div>:''}
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleFieldChange} placeholder="Name" name="name" required type="text" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={starts} onChange={handleFieldChange} name="starts" required type="date" id="starts" className="form-control"/>
                <label htmlFor="ends">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input value={ends} onChange={handleFieldChange} name="ends" required type="date" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="form mb-3">
                <label htmlFor="description">Description</label>
                <textarea value={description} onChange={handleFieldChange} placeholder="describe the event in a few sentences" name="description" id="description" className="form-control" rows="4"></textarea>
              </div>
              
              <div className="form-floating mb-3">
                <input value={max_presentations} onChange={handleFieldChange} placeholder="Maximum presentations" name="max_presentations" required type="number" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Maximum Presentations</label>
              </div>

              <div className="form-floating mb-3">
                <input value={max_attendees} onChange={handleFieldChange} placeholder="Maximum Attendees" name="max_attendees" required type="number" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Maximum Attendees</label>
              </div>

              <div className="mb-3">
                <select value={location} onChange={handleFieldChange} name="location" required id="location" className="form-select">
                  {locations.map(location => <option key={location.id} value={location.id}> {location.name} </option>)}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
