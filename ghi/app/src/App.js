import logo from './logo.svg';
import './App.css';
import Nav from './Nav.js'
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AttendConferenceForm from './AttendConferenceForm';
import MainPage from './MainPage';
import PresentationForm from './PresentationForm';

function App({ attendees }) {
  if (attendees === undefined) return null;
  return (
    <div>
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route index element={<MainPage />} />
            <Route path="conferences"> 
                <Route path="new" element={<ConferenceForm />} />
            </Route>
            <Route path="locations"> 
                <Route path="new" element={<LocationForm />} />
            </Route>
            <Route path="attendees"> 
                <Route path="" element={<AttendeesList attendees={attendees} />} />
                <Route path="new" element={<AttendConferenceForm />} />
            </Route>
            <Route path="presentations"> 
                <Route path="new" element={<PresentationForm />} />
            </Route>
          </Routes>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;

