import React, { useState, useEffect } from 'react';

function LocationForm() {
    const [location, setLocation] = useState('');
    const [states, setStates] = useState([]);

    const [name, setName] = useState('');
    const [roomCount, setRoomCount] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');

    const handleFieldChange = (e) => {
        const fieldName = e.target.name[0].toUpperCase() + e.target.name.slice(1);
        const value = e.target.value;
        eval(`set${fieldName}(value)`);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.room_count = roomCount;
        data.name = name;
        data.city = city;
        data.state = state;

        console.log(data);
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            setName('');
            setRoomCount('');
            setCity('');
            setState('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setStates(data.states);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new location</h1>
              <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input value={name} onChange={handleFieldChange} placeholder="Name" name="name" required type="text" id="name" className="form-control" />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={roomCount} onChange={handleFieldChange} placeholder="Room count" name="roomCount" required type="number" id="room_count" className="form-control"/>
                  <label htmlFor="room_count">Room count</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={city} onChange={handleFieldChange} placeholder="City" name="city" required type="text" id="city" className="form-control"/>
                  <label htmlFor="city">City</label>
                </div>
                <div className="mb-3">
                  <select value={state} onChange={handleFieldChange} name="state" required id="state" className="form-select">
                        {states.map(state => <option key={state.abbreviation} value={state.abbreviation}> {state.name} </option>)}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
      </div>
    );
}

export default LocationForm;
