import React, { useState, useEffect } from 'react';

function PresentationForm() {
    const [conferences, setConferences] = useState([]);

    const [success, setSuccess] = useState(false);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [company, setCompany] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('');

    const [presentation, setPresentation] = useState('');

    const handleFieldChange = (e) => {
        const fieldName = e.target.name[0].toUpperCase() + e.target.name.slice(1);
        const value = e.target.value;
        eval(`set${fieldName}(value)`);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.synopsis = synopsis;
        data.presenter_name = name;
        data.presenter_email = email;
        data.company_name = company;
        data.title = title;

        console.log(data);
        const conferenceId = /(?<=\/)\d*(?=\/)/gm.exec(conference)[0]
        const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);
            setSuccess(true);
            setPresentation(newPresentation);

            setName('');
            setEmail('');
            setCompany('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            {success ? 
            <div className="alert alert-success d-flex align-items-center" role="alert">
                <div>
                  `Congratulations: created new presentation {presentation.title}`
                </div>
            </div>:''}
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleFieldChange} placeholder="Presenter name" name="name" required type="text" id="name" className="form-control"/>
                <label htmlFor="name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={email} onChange={handleFieldChange} placeholder="Presenter email" name="email" required type="text" id="email" className="form-control"/>
                <label htmlFor="email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input value={company} onChange={handleFieldChange} placeholder="Company name" name="company" required type="text" id="company" className="form-control"/>
                <label htmlFor="company">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={title} onChange={handleFieldChange} placeholder="Title" name="title" required type="text" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="form mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea value={synopsis} onChange={handleFieldChange} placeholder="describe the presentation in a few sentences" name="synopsis" id="synopsis" className="form-control" rows="4"></textarea>
              </div>
              <div className="mb-3">
                <select value={conference} onChange={handleFieldChange} name="conference" required id="conference" className="form-select">
                  {conferences.map(conference => <option key={conference.href} value={conference.href}> {conference.name} </option>)}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default PresentationForm;
